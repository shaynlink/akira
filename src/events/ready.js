'use strict';
/**
 * Event class
 */
class Ready {
  /**
    * Launch script
    */
  launch() {
    console.log(`${this.user.username} is ready !`);
    this.jpop.broadcast = this.voice.createBroadcast();
    this.jpop.dispatcher = this.jpop.broadcast.play('https://listen.moe/stream');
    this.kpop.broadcast = this.voice.createBroadcast();
    this.kpop.dispatcher = this.kpop.broadcast.play('https://listen.moe/kpop/stream');
  };
};

module.exports = Ready;
