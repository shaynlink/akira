'use strict';
const language = require('./../translate');
/**
 * Event class
 */
class Message {
  /**
   * @param {*} message - Event message
   * @return {*}
   */
  async launch(message) {
    if (message.author.bot) return false;
    if (message.guild) {
      // eslint-disable-next-line no-var
      var guild = await this.getGuild(message.guild);
      if (!guild) {
        guild = await this.createGuild({
          name: message.guild.name,
          guildID: message.guild.id,
        });
      };
      await this.updateGuild(message.guild, {
        xp: Number(guild.xp) + 1,
      });
    };
    // eslint-disable-next-line no-var
    var user = await this.getUser(message.author);
    if (!user) {
      user = await this.createUser({
        name: message.author.username,
        userID: message.author.id,
      });
    };
    await this.updateUser(message.author, {
      xp: Number(user.xp) + 1,
    });

    if (!message.content.startsWith(this.config.prefix)) return false;
    const query = message.content
        .slice(this.config.prefix.length)
        .trim()
        .split(/ +/g);
    const command = query.shift().toLowerCase();
    if (!this.commands.has(command) && !this.aliases.has(command)) {
      return message.reply(language(guild.lg, 'command_not_found'));
    };
    const cmd = this.commands.get(command) ||
      this.commands.get(this.aliases.get(command));
    if (cmd.conf.nsfw && !message.channel.nsfw) {
      return message.reply(language(guild.lg, 'command_nsfw_not_authorized'));
    };
    if (!cmd.conf.guildOnly && !message.channel.guild) {
      return message.reply(language(guild.lg, 'command_dm_not_authorized'));
    };
    if (!cmd.conf.enable) {
      return message.reply(language(guild.lg, 'command_disable'));
    }
    cmd.launch(message, query, {user, guild});
  };
};

module.exports = Message;
