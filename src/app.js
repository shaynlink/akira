'use strict';
const {Client, Collection} = require('discord.js');
const {readdirSync} = require('fs');
const {DISCORD_TOKEN} = require('./../configuration');
const {JpopClient, KpopClient} = require('./websocket');
/**
 * Class Akira exents Client
 * @class
 */
class Akira extends Client {
  /**
  * Consctructor options https://discord.js.org/#/docs/main/master/typedef/ClientOptions
  */
  constructor() {
    super();
    this.commands = new Collection();
    this.aliases = new Collection();
    this.config = {
      prefix: 'A!',
    };
    this.mongoose = require('./database/mongoose');
    this.music = {};
    this.jpop = {
      broadcast: null,
      dispatcher: null,
      ws: new JpopClient(),
      data: null,
    };
    this.kpop = {
      broadcast: null,
      dispatcher: null,
      ws: new KpopClient(),
      data: null,
    };
  };
  /**
  * Load events file
  * @param {String} eventPath - path to event
  */
  loadEvent(eventPath) {
    try {
      const RequireEvent = require(`./events/${eventPath}`);
      const event = new RequireEvent(this);
      const eventName = eventPath.split('.')[0];
      this.on(eventName, event.launch.bind(this));
    } catch (error) {
      console.error(error);
    };
  };
  /**
   * Load commands file
   * @param {String} commandPath - path to command
   */
  loadCommand(commandPath) {
    try {
      const RequireCommand = require(`./commands/${commandPath}`);
      const command = new RequireCommand(this);
      this.commands.set(command.help.name, command);
      command.conf.aliases.forEach((alias) => {
        this.aliases.set(alias, command.help.name);
      });
    } catch (error) {
      console.error(error);
    };
  };
};

const client = new Akira();
require('./database/functions')(client);

const eventFiles = readdirSync('./src/events/');
const commandFiles = readdirSync('./src/commands/');

// eslint-disable-next-line guard-for-in
for (const file in eventFiles) {
  client.loadEvent(eventFiles[file]);
};
// eslint-disable-next-line guard-for-in
for (const file in commandFiles) {
  client.loadCommand(commandFiles[file]);
};

client.mongoose.init();
client.login(DISCORD_TOKEN).catch(console.error);

client.jpop.ws.connect();
client.jpop.ws.on('event', (data) => client.jpop.data = data);
client.jpop.ws.on('open', () => console.log('Jpop broadcast connected'));
client.jpop.ws.on('close', () => console.log('Jpop broadcast disconnected'));

client.kpop.ws.connect();
client.kpop.ws.on('event', (data) => client.kpop.data = data);
client.kpop.ws.on('open', () => console.log('Kpop broadcast connected'));
client.kpop.ws.on('close', () => console.log('Kpop broadcast disconnected'));
