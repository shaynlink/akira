'use strict';
const Command = require('../plugin/Command');
const language = require('./../translate');
const ytdl = require('ytdl-core');
const {YOUTUBE_KEY} = require('./../../configuration');
const axios = require('axios');
const {MessageCollector} = require('discord.js');

/**
 * Command class
 */
class Play extends Command {
  /**
   * @param {*} client - Client
   */
  constructor(client) {
    super(client, {
      name: 'play',
      category: 'music',
      description: 'command_play_description',
      usage: 'play [title song | url youtube]',
      nsfw: false,
      enable: true,
      guildOnly: true,
      alias: [],
    });
    this.client = client;
  };
  /**
   * @param {*} message - message
   * @param {array} query - argument
   */
  async launch(message, query, {guild}) {
    const dj = message.guild.roles.cache.find((role) =>
      role.name.trim().toLowerCase() === 'dj' );
    if (!message.member.voice.channel) {
      return message.reply(language(guild.lg, 'command_music_userNoJoin'));
    };
    if (message.guild.me.voice.channel &&
      message.guild.me.voice.channel.members.length < 2 &&
      !message.member.hasPermission(['ADMINISTRATOR'],
          {checkAdmin: true, checkOwner: true}) &&
          !message.member.roles.cache.some((role) => role.id === dj.id)) {
      return message.reply(language(guild.lg, 'command_music_occured'));
    };
    if (!this.client.music[message.guild.id]) {
      this.client.music[message.guild.id] = {
        connection: null,
        dispatcher: null,
        index: 0,
      };
    };
    this.client.music[message.guild.id].connection =
      await message.member.voice.channel.join();
    if (!query.join(' ')) {
      this.client.music[message.guild.id].dispatcher =
        this.client.music[message.guild.id].connection.play(
            ytdl(`https://www.youtube.com/watch?v=${guild.music_hist[0].id.videoId}`, {
              filter: 'audioonly',
            }, {
              volume: guild.music_volume,
            }),
        );
      if (!guild.music_muteIndication) {
        return message.channel.send({
          embed: {
            title: language(guild.lg, 'commande_music_played'),
            description: guild.music_hist[0].snippet.title,
            thumbnail: {
              url: guild.music_hist[0].snippet.thumbnails.default.url,
            },
          },
        });
      };
    };
    const youtube = await axios.get(`https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=5&key=${YOUTUBE_KEY}&q=${encodeURI(query.join(' '))}`).then((response) => response.data);
    if (youtube.error) return message.channel.send(youtube.error.message);
    const content = {embed: {
      title: language(guild.lg, 'command_music_listMusic'),
      color: guild.color,
      description: `${youtube.items.map((v, i) =>
        `[${i+1}] ${v.snippet.title}`).join('\n')}`,
      timestamp: Date.now(),
      footer: {
        text: language(guild.lg, 'command_music_choiceFooter'),
        icon_url: this.client.user
            .displayAvatarURL({format: 'webp', dynamic: true, size: 2048}),
      },
    }};
    message.channel.send(content).then((msg) => {
      const filter = (msg) => msg.author.id === message.author.id;
      const collector = new MessageCollector(message.channel, filter, {
        time: 20000,
      });
      collector.on('collect', async (msgCollected) => {
        const choice = msgCollected.content.trim().split()[0];
        if (choice.toLowerCase() === 'cancel') {
          return collector.stop('STOPPED');
        };
        if (!choice || isNaN(choice)) {
          return message.channel.send(
              language(guild.lg, 'command_music_choiceInvalid'),
          );
        };
        if (choice > youtube.items.length || choice <= 0) {
          return message.reply(
              language(guild.lg, 'command_music_choiceNotFound'),
          );
        };
        const song = youtube.items[choice - 1];
        collector.stop('PLAY');
        msg.delete();
        msgCollected.delete();
        guild.music_hist.push(song);
        await this.client.updateGuild(message.guild, {
          music_hist: guild.music_hist,
        });
        guild = await this.client.getGuild(message.guild);
        if (guild.music_hist.length > 1 &&
          this.client.music[message.guild.id].dispatcher) {
          return message.reply({embed: {
            title: language(guild.lg, 'command_music_addPlaylist'),
            description: song.snippet.title,
            thumbnail: {
              url: song.snippet.thumbnails.default.url,
            },
          },
          });
        } else if (guild.music_hist.length > 1 &&
          !this.client.music[message.guild.id].dispatcher) {
          message.channel.send({embed: {
            title: language(guild.lg, 'command_music_addPlaylist'),
            description: song.snippet.title,
            thumbnail: {
              url: song.snippet.thumbnails.default.url,
            },
          },
          });
        };
        this.client.music[message.guild.id].dispatcher =
        this.client.music[message.guild.id].connection.play(
            ytdl(`https://www.youtube.com/watch?v=${guild.music_hist[0].id.videoId}`, {
              filter: 'audioonly',
            }, {
              volume: guild.music_volume,
            }),
        );
        if (!guild.music_muteIndication) {
          return message.channel.send({
            embed: {
              title: language(guild.lg, 'commande_music_played'),
              description: guild.music_hist[0].snippet.title,
              thumbnail: {
                url: guild.music_hist[0].snippet.thumbnails.default.url,
              },
            },
          });
        } else {
          message.react('👌');
        };
        this.client.music[message.guild.id]
            .dispatcher.on('finish', async () => {
              console.log('finish');
              this.client.music[message.guild.id].dispatcher = null;
              if (guild.music_loop === 'off' && guild.music_hist.length === 0) {
                return message.channel.send(
                    language(guild.lg, 'command_music_finish'),
                );
              } else if (guild.music_loop === 'off' &&
                !guild.music_hist.length === 0) {
                guild.music_hist.shift();
                this.client.updateGuild(message.guild, {
                  music_hist: guild.music_hist,
                });
                guild = await this.client.getGuild(message.guild);
                this.client.music[message.guild.id].connection.play(
                    ytdl(`https://www.youtube.com/watch?v=${guild.music_hist[0].id.videoId}`, {
                      filter: 'audioonly',
                    }, {
                      volume: guild.music_volume,
                    }),
                );
                if (!guild.music_muteIndication) {
                  return message.channel.send({
                    embed: {
                      title: language(guild.lg, 'commande_music_played'),
                      description: guild.music_hist[0].snippet.title,
                      thumbnail: {
                        url: guild.music_hist[0].snippet.thumbnails.default.url,
                      },
                    },
                  });
                };
              } else if (guild.music_loop === 'on') {
                if (this.client.music[message.guild.id].index ===
                  guild.music_hist.length-1) {
                  this.client.music[message.guild.id].index = 0;
                } else {
                  this.client.music[message.guild.id].index++;
                };
                this.client.music[message.guild.id].dispatcher =
                this.client.music[message.guild.id].connection.play(
                    ytdl(`https://www.youtube.com/watch?v=${guild.music_hist[this.client.music[message.guild.id].index].id.videoId}`, {
                      filter: 'audioonly',
                    }, {
                      volume: guild.music_volume,
                    }),
                );
                if (!guild.music_muteIndication) {
                  return message.channel.send({
                    embed: {
                      title: language(guild.lg, 'commande_music_played'),
                      description: guild.music_hist[
                          this.client.music[message.guild.id].index
                      ].snippet.title,
                      thumbnail: {
                        url: guild.music_hist[
                            this.client.music[message.guild.id].index
                        ].snippet.thumbnails.default.url,
                      },
                    },
                  });
                };
              } else if (guild.music_loop === 'once') {
                this.client.music[message.guild.id].dispatcher =
                this.client.music[message.guild.id].connection.play(
                    ytdl(`https://www.youtube.com/watch?v=${guild.music_hist[0].id.videoId}`, {
                      filter: 'audioonly',
                    }, {
                      volume: guild.music_volume,
                    }),
                );
                if (!guild.music_muteIndication) {
                  return message.channel.send({
                    embed: {
                      title: language(guild.lg, 'commande_music_played'),
                      description: guild.music_hist[0].snippet.title,
                      thumbnail: {
                        url: guild.music_hist[0].snippet.thumbnails.default.url,
                      },
                    },
                  });
                };
              };
            });
        this.client.music[message.guild.id].dispatcher.on('error', (err) => {
          console.error(err);
          guild.music_hist.shift();
          this.client.updateGuild(message.guild, {
            music_hist: guild.music_hist,
          });
          guild = this.client.getGuild(message.guild);
          this.client.music[message.guild.id].connection.play(
              ytdl(`https://www.youtube.com/watch?v=${guild.music_hist[0].id.videoId}`, {
                filter: 'audioonly',
              }, {
                volume: guild.music_volume,
              }),
          );
          if (!guild.music_muteIndication) {
            return message.channel.send({
              embed: {
                title: language(guild.lg, 'commande_music_played'),
                description: guild.music_hist[0].snippet.title,
                thumbnail: {
                  url: guild.music_hist[0].snippet.thumbnails.default.url,
                },
              },
            });
          };
          return message.channel.send(err, {code: 'js'});
        });
      });

      collector.on('end', (collected, reason) => {
        if (reason === 'STOPPED') {
          return message.reply(language(guild.lg, 'command_music_choiceStop'));
        } else if (reason === 'PLAY') {
          return false;
        } else {
          return message.reply(language(guild.lg, 'command_music_timeout'));
        };
      });
    });
  };
};

module.exports = Play;
