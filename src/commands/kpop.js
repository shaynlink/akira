'use strict';
const Command = require('../plugin/Command');
const language = require('../translate');

/**
 * Command class
 */
class Kpop extends Command {
  /**
   * @param {*} client - Client
   */
  constructor(client) {
    super(client, {
      name: 'kpop',
      category: 'music',
      description: 'command_broadcast_kpop_description',
      usage: 'kpop',
      nsfw: false,
      enable: true,
      guildOnly: true,
      alias: [],
    });
    this.client = client;
  };
  /**
   * @param {*} message - message
   * @param {array} query - arguments
   * @return {any}
   */
  async launch(message, query, {guild}) {
    const dj = message.guild.roles.cache.find((role) =>
      role.name.trim().toLowerCase() === 'dj' );
    if (!message.member.voice.channel) {
      return message.reply(language(guild.lg, 'command_music_userNoJoin'));
    };
    if (message.guild.me.voice.channel &&
      message.guild.me.voice.channel.members.length < 2 &&
      !message.member.hasPermission(['ADMINISTRATOR'],
          {checkAdmin: true, checkOwner: true}) &&
          !message.member.roles.cache.some((role) => role.id === dj.id)) {
      return message.reply(language(guild.lg, 'command_music_occured'));
    };
    if (!this.client.music[message.guild.id]) {
      this.client.music[message.guild.id] = {
        connection: null,
        broadcast: true,
      };
    };
    this.client.music[message.guild.id].connection =
      await message.member.voice.channel.join();
    this.client.music[message.guild.id].connection
        .play(this.client.kpop.broadcast);
    return message.channel.send(language(guild.lg,
        'command_broadcast_actualPlaying')
        .replace('{{title}}', this.client.kpop.data.song.title));
  };
};

module.exports = Kpop;
