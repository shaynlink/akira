'use strict';
const Command = require('../plugin/Command');
const language = require('./../translate');

/**
 * Command class
 */
class Kick extends Command {
  /**
   * @param {*} client - Client
   */
  constructor(client) {
    super(client, {
      name: 'kick',
      category: 'moderation',
      description: 'command_kick_description',
      usage: 'kick (username | nickname | id) [reason]',
      nsfw: false,
      enable: true,
      guildOnly: true,
      alias: [],
    });
    this.client = client;
  };
  /**
   * @param {*} message - message
   * @param {array} query - arguments
   * @return {any}
   */
  launch(message, query, {guild}) {
    if (!message.member.hasPermission(['KICK_MEMBERS'],
        {checkAnime: true, checkOwner: true})) {
      return message.reply(language(guild.lg, 'member_missing_permission'));
    };
    const queryMember = query.shift();
    const member = message.mentions.members.first() ||
        message.guild.member(
            message.mentions.users.first(),
        ) ||
        message.guild.members.cache.find((member) =>
          member.id === queryMember) ||
        message.guild.members.cache.find((member) =>
          member.displayName === queryMember) ||
        message.guild.members.cache.find((member) =>
          member.user.username === queryMember) ||
        message.guild.members.cache.find((member) =>
          member.toString() === queryMember);
    if (!member) {
      return message.reply(language(guild.lg, 'member_not_found'));
    };
    if (message.member.roles.highest.position < member.roles.highest.position) {
      return message.reply(language(guild.lg, 'command_kick_invalidPosition'));
    };
    if (message.guild.me.roles.highest.position <
        member.roles.highest.posisiton) {
      return message.reply(
          language(guild.lg, 'command_kick_MeInvalidePosition'),
      );
    };
    let errored = false;
    member.kick({reason: query.join(' ')}).catch((err) => {
      console.log(err);
      errored = true;
      message.channel.send(language(guild.lg, 'command_kick_error'));
      return message.channel.send(err, {code: 'js'});
    }).then(() => {
      console.log(errored);
      if (!errored) {
        message.channel.send(language(guild.lg, 'command_kick_success'));
      };
    });
  };
};

module.exports = Kick;
