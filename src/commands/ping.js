'use strict';
const Command = require('../plugin/Command');
const language = require('./../translate');

/**
 * Command class
 */
class Ping extends Command {
  /**
   * @param {*} client - Client
   */
  constructor(client) {
    super(client, {
      name: 'ping',
      category: 'util',
      description: 'command_ping_description',
      usage: 'ping',
      nsfw: false,
      enable: true,
      guildOnly: false,
      alias: ['pg'],
    });
    this.client = client;
  };
  /**
   * @param {*} message - message
   */
  launch(message) {
    console.log(language('fr', 'world_channel'));
    message.reply(`🏓 Pong ! - ${this.client.ws.ping} ms`).catch(console.error);
  };
};

module.exports = Ping;
