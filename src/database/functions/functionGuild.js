const mongoose = require('mongoose');
const {Guild} = require('./../lib/Guild');

module.exports = (client) => {
  client.getGuild = async (guild) => {
    const data = await Guild.findOne({guildID: guild.id});
    if (!data) return null;
    return data;
  };

  client.updateGuild = async (guild, settings) => {
    let data = await client.getGuild(guild);
    if (typeof data !== 'object') data = {};
    for (const key in settings) {
      if (data[key] !== settings[key]) data[key] = settings[key];
    }
    return data.updateOne(settings);
  };

  client.createGuild = async (settings) => {
    // eslint-disable-next-line new-cap
    const merged = Object.assign({_id: mongoose.Types.ObjectId()}, settings);
    const createGuild = await new Guild(merged);
    await createGuild.save().then((g) => console.log(`New guild -> ${g.name}.`))
        .finally(() => {
          return client.getGuild({guildID: settings.guildID});
        });
  };
};
