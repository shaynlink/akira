'use strict';
const mongoose = require('mongoose');

const guildShema = {
  name: String,
  guildID: Number,
  prefix: {
    'type': String,
    'default': 'A!',
  },
  color: {
    'type': String,
    'default': '#F1C110',
  },
  xp: {
    'type': Number,
    'default': 0,
  },
  lg: {
    'type': String,
    'default': 'en',
  },
  banner: {
    'type': Number,
    'default': 0,
  },
  welcome_banner: {
    'type': String,
    'default': 'default',
  },
  welcome_channel: {
    'type': String,
    'default': 'default',
  },
  goodbye_banner: {
    'type': String,
    'default': 'default',
  },
  goodbye_channel: {
    'type': String,
    'default': 'default',
  },
  welcome_active: {
    'type': Boolean,
    'default': false,
  },
  goodbye_active: {
    'type': Boolean,
    'default': Boolean,
  },
  music_hist: {
    'type': Array,
    'default': [
      Object,
    ],
  },
  music_volume: {
    'type': Number,
    'default': 50,
  },
  music_loop: {
    'type': String,
    'default': 'off',
  },
  music_muteIndication: {
    'type': Boolean,
    'default': false,
  },
};

module.exports.Guild = mongoose.model('Guild', guildShema);
