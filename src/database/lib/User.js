'use strict';
const mongoose = require('mongoose');

const userShema = {
  name: String,
  userID: String,
  xp: {
    'type': Number,
    'default': 0,
  },
  prism: {
    'type': Number,
    'default': 100,
  },
  item: [
    {
      id: String,
      name: String,
      valeur: String,
    },
  ],
  custome: {
    banner: {
      'type': Number,
      'default': 0,
    },
    loader: {
      'type': Number,
      'default': 0,
    },
  },
};

module.exports.User = mongoose.model('User', userShema);
