'use strict';
module.exports = {
  search(lg) {
    return this.data[lg];
  },
  data: {
    'command_not_found': 'This command does not exist',
    'command_ping_description': 'Displays bot pings',
    'command_config_description': 'Configure the bot by adding to the guild',
    'command_config_title': 'Control panel, prefix: {{prefix}}',
    'command_config_description':
      'Here is the bot configuration for your server',
    'command_config_keyNotMatch':
      // eslint-disable-next-line max-len
      '**{{key}}** is not a configuration item, please write it correctly using capital letters',
    'command_config_listSetting': 'here is a list of settings: `{{key}}`',
    'command_config_successUpdate': 'Modification successful -> {{map}}',
    'command_ban_description': 'Ban a member from the server',
    'command_ban_invalidPosition':
      'You are not in the ban position of this user',
    'command_ban_MeInvalidePosition': 'I am not able to ban this user',
    'command_ban_success': 'User has been banned',
    'command_ban_error': 'I couldn\'t ban this user',
    'command_kick_description': 'Kick a member from the server',
    'command_kick_invalidPosition':
      'You are not in the Kick position of this user',
    'command_kick_MeInvalidePosition': 'I am not able to Kick this user',
    'command_kick_success': 'User has been Kicked',
    'command_kick_error': 'I couldn\'t Kick this user',
    'command_help_description': 'Show all bot commands',
    'command_help_title': 'Here is my list of all my orders',
    'command_help_command': 'Here is the order information',
    'command_music_userNoJoin': 'You are not connected to a voice channel',
    'command_music_occured': 'I\'m already in a voice channel',
    'command_music_finish': 'The playlist is finished',
    'command_music_listMusic': 'List of music',
    'command_music_choiceFooter': 'Send `cancel` for cancel :p',
    'command_music_choiceInvalid': 'This choice is not valid',
    'command_music_choiceNotFound': 'This choice is not part of the selection',
    'command_music_addPlaylist': 'music added to the playlist',
    'command_music_choiceStop': 'You canceled the selection',
    'command_music_timeout': 'You have not selected music in time',
    'commande_music_played': 'I played now',
    'command_music_matchChannel': 'I\'m already in your voice room',
    'command_nsfw_not_authorized':
      'You must be in an nsfw salon to execute this command',
    'command_dm_not_authorized':
      'You must be in a guild to execute this command',
    'command_disable': 'This command is disabled',
    'command_broadcast_jpop_description':
      'Connects to a Jpop radio',
    'command_broadcast_kpop_description':
      'Connects to a Kpop radio',
    'command_broadcast_actualPlaying':
      'Current music is {{title}}',
    'custom_channel_welcome': 'Welcome {{name} in {{guild}}',
    'custom_channel_goodbye': '{{name}} left us',
    'world_prefix': 'prefix',
    'world_language': 'language',
    'world_color': 'color',
    'world_channel': 'channel',
    'world_welcome': 'welcome',
    'world_goodbye': 'goodbye',
    'world_active': 'active',
    'world_command': 'commands',
    'member_missing_permission': 'You do not have the necessary permissions',
    'member_not_found': 'I cannot find the member in the server',
  },
};
