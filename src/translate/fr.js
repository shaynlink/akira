'use strict';
module.exports = {
  search(lg) {
    return this.data[lg];
  },
  data: {
    'command_not_found': 'Cette commande n\'existe pas',
    'command_ping_description': 'Permet d\'afficher les pings du bot',
    'command_config_description': 'Configurer le bot par apport à la guilde',
    'command_config_title': 'Panneau de configuration, prefix: {{prefix}}',
    'command_config_description':
      'Voici la configuration du bot pour votre serveur',
    'command_config_keyNotMatch':
      // eslint-disable-next-line max-len
      '**{{key}}** n\'est pas un élément de configuration, veuillez l\'écrire correctement en respectant les majuscules',
    'command_config_listSetting': 'Voicie la liste des paramètres: `{{key}}`',
    'command_config_successUpdate': 'Modification réussie -> {{map}}',
    'command_ban_description': 'Ban un membre du serveur',
    'command_ban_invalidPosition':
      'Vous n\'êtes pas en position de ban cet utilisateur',
    'command_ban_MeInvalidePosition':
      'Je ne suis pas en mesure de ban cet utilisateur',
    'command_ban_success': 'L\'utilisateur a était bannie',
    'command_ban_error': 'Je n\'ai pas pu ban cet utilisateur',
    'command_kick_description': 'Kick un membre du serveur',
    'command_kick_invalidPosition':
      'Vous n\'êtes pas en position de kick cet utilisateur',
    'command_kick_MeInvalidePosition':
      'Je ne suis pas en mesure de kick cet utilisateur',
    'command_kick_success': 'L\'utilisateur a était kick',
    'command_kick_error': 'Je n\'ai pas pu kick cet utilisateur',
    'command_help_description': 'Affiche toute les commandes du bot',
    'command_help_title': 'Voici ma liste de toute mes commandes',
    'command_help_command': 'Voici les informations de la commande',
    'command_music_choiceFooter': 'Envoyer `cancel` pour annuler',
    'command_music_userNoJoin': 'Vous n\'êtes pas connecté à un salon vocal',
    'command_music_occured': 'Je suis déjà dans un salon vocal',
    'command_music_finish': 'La playlist est terminé',
    'command_music_listMusic': 'Liste des musiques',
    'command_music_choiceInvalid': 'Ce choix n\'est pas valide',
    'command_music_choiceNotFound':
      'Ce choix ne fait pas parti de la sélection',
    'command_music_addPlaylist': 'musique ajouté à la playlist',
    'command_music_choiceStop': 'Vous avez annulé la selection',
    'command_music_timeout': 'Vous n\'avez pas sélectionné de musique à temp',
    'commande_music_played': 'Je joue maintenant',
    'command_music_matchChannel': 'Je suis déjà présent dans votre salon vocal',
    'command_nsfw_not_authorized':
      'Vous devez être dans un salon nsfw pour exécuter cette commande',
    'command_dm_not_authorized':
      'Vous devez être dans un serveur pour exécuter cette commande',
    'command_disable': 'Cette commande est désactivé',
    'command_broadcast_jpop_description':
      'Se connecte à une radio Jpop',
    'command_broadcast_kpop_description':
      'Se connecte à une radio Kpop',
    'command_broadcast_actualPlaying':
      'La musique actuel est {{title}}',
    'custom_channel_welcome': 'Bienvenue {{name} chez {{guild}}',
    'custom_channel_goodbye': '{{name}} nous a quitté',
    'world_prefix': 'prefix',
    'world_language': 'langage',
    'world_color': 'couleur',
    'world_channel': 'salon',
    'world_welcome': 'bienvenue',
    'world_goodbye': 'au revoir',
    'world_active': 'active',
    'world_command': 'commandes',
    'member_missing_permission': 'Vous n\'avez pas les permissions nécéssaires',
    'member_not_found': 'Je ne trouve pas le membre dans le serveur',
  },
};
